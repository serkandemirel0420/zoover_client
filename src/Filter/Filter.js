import React, {Component} from 'react';
import './Filter.css';

import Select from 'react-select';

// Be sure to include styles at some point, probably during your bootstrapping
import 'react-select/dist/react-select.css';

class FilterComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {value: "one"};
    }

    passDataToParent(e) {
        this.setState({value: (e ? e.value : "")});
        this.props.filter((e ? e.value : ""))
    }

    render() {
        return (
            <Select
                name="traveledWidth"
                placeholder={"Filter By Traveled With"}
                value={this.state.value}
                options={this.props.items}
                onChange={this.passDataToParent.bind(this)}
                inputProps={{'id': "traveledWith"}}
                searchable={false}
            />
        )
    }
}

export default FilterComponent