import React, {Component} from 'react';
import './reviews.css';
import Average from '../Average/Average'
import ReactPaginate from 'react-paginate';
import  Filter from '../Filter/Filter'
import  Item from '../Item/Item'
import Order from '../Order/Order'

import axios from 'axios';

class ItemsComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            reviews: null,
            traveledWith: null,
            avg: null
        };
    }

    loadData(offset = 0) {

        var _this = this;

        var traveledWith = this.state.traveledWith;
        traveledWith = (traveledWith ? "&traveledWith=" + traveledWith : "");
        var orderBy = this.state.order;
        orderBy = (orderBy ? orderBy : "");

        axios
            .get('http://localhost:3001/review?offset=' + offset + traveledWith + orderBy)
            .then((result) => {
                var reviews = result.data;
                _this.setState({
                    reviews
                });
                window.scrollTo(0, 0)
            });

    }

    componentDidMount() {
        this.loadData()
    }

    filterByTraveledWidth(value) {
        this.setState({
            current: 0,
            traveledWith: value,
        }, this.loadData)
    }

    length() {
        return this.state.reviews.weightedRatingData.length
    }

    //page number click in pagination
    handlePageClick(e) {
        this.setState({
            current: e.selected
        });

        let count = (e.selected ) * 10;
        this.loadData(count);
    }

    orderByClick(value) {
        this.setState({
            current: 0,
            order: value
        }, this.loadData);
    }


    render() {

        if (!this.state.reviews) {
            return <div>Loading...</div>
        }

        return (
            <div id="reviews">
                <h2>Reviews</h2>

                <Average average={this.state.reviews.avg}/>

                <div className="manageRevievs">
                    <Filter items={this.state.reviews.uniqueTravelWidth}
                            filter={this.filterByTraveledWidth.bind(this)}/>

                    <Order id={"orderBy"}
                           change={this.orderByClick.bind(this)}
                           text={"Order By Review Date"}/>

                </div>


                {this.state.reviews.weightedRatingData.map((item, i) =>
                    <Item key={item.id} values={item}/>
                )}


                <ReactPaginate previousLabel={"previous"}
                               nextLabel={"next"}
                               breakLabel={<a >...</a>}
                               breakClassName={"break-me"}
                               pageCount={(this.state.reviews.length / 10)}
                               marginPagesDisplayed={2}
                               pageRangeDisplayed={4}
                               onPageChange={this.handlePageClick.bind(this)}
                               containerClassName={"pagination"}
                               subContainerClassName={"pages pagination"}
                               activeClassName={"active"}
                               forcePage={this.state.current}/>
            </div>
        );
    }
}

export default ItemsComponent;
