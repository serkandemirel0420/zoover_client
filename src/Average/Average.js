import React, {Component} from 'react';
import './Average.css';

class AverageComponent extends Component {

    roundNumberToOneDecimal(number) {
        return Math.round(number * 10) / 10;
    }

    render() {
        return (
            <div>

                <div className="generalAverage">
                    <h3 className="totalAverageHeader"> Total Averages</h3>
                    <div className="general">
                        <span className="bold"> General : </span>
                        { this.roundNumberToOneDecimal(this.props.average.general.general)}</div>
                    <div className="location">
                        <span className="bold"> Location :</span>
                        {this.roundNumberToOneDecimal(this.props.average.aspects.location)}</div>
                    <div className="priceQuality">
                        <span className="bold"> Price :</span>
                        {this.roundNumberToOneDecimal(this.props.average.aspects.priceQuality)}</div>
                    <div className="restaurants">
                        <span className="bold">  Restaurants : </span>
                        {this.roundNumberToOneDecimal(this.props.average.aspects.restaurants)}</div>
                </div>
            </div>
        );
    }
}

export default AverageComponent;
