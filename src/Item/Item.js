/**
 * Created by serkandemirel on 14/05/2017.
 */
import React, {Component} from 'react';
import './Item.css'

class Item extends Component {

    dateConvert(date) {

        var monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        var d = new Date(date);
        return monthNames[d.getMonth()] + " " + d.getFullYear();
        // TODO convert to string literal
    }

    render() {
        return (
            <div className="review" key={this.props.values.id}>
                <h3 className="title">  { (Object.values(this.props.values.titles)[0] || "- no title - ") }</h3>
                <div className="ratings">
                    <div className="item general">
                        <span className="bold"> General : </span>
                        {this.props.values.ratings.general.general}</div>
                    <div className="item location">
                        <span className="bold">  Location : </span>
                        {this.props.values.ratings.aspects.location}</div>
                    <div className="item priceQuality">
                        <span className="bold"> Price Quality : </span>
                        {this.props.values.ratings.aspects.priceQuality}</div>
                    <div className="item restaurants">
                        <span className="bold">  Restaurants : </span>
                        {this.props.values.ratings.aspects.restaurants}</div>
                </div>
                <div className="text">  {(this.props.values.texts.nl || " - no comment - " )} </div>
                <br/>
                <div className="travelDate date">Travelled in &nbsp;
                    <span className="bold">
                        {this.dateConvert(this.props.values.travelDate) }
                    </span>
                </div>
                <span> - </span>
                <div className="entryDate date">Revieved in &nbsp;
                    <span className="bold">
                     { this.dateConvert(this.props.values.entryDate) }
                    </span>
                </div>
                <hr />

            </div>
        )


    }

}

export default Item;