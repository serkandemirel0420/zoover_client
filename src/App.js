import React, {Component} from 'react';
import Reviews from './Reviews/reviews';
import './App.css';

class App extends Component {

    render() {
        return (
            <div className="App">
                <Reviews/>
            </div>
        );
    }
}

export default App;
