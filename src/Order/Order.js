/**
 * Created by serkandemirel on 15/05/2017.
 */

import React, {Component} from 'react';
import './Order.css';

import Select from 'react-select';

// Be sure to include styles at some point, probably during your bootstrapping
import 'react-select/dist/react-select.css';


class OrderByComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {value: "one"}
    }

    //passing to root
    handleSortChange(e) {
        this.setState({value: (e ? e.value : "")});
        this.props.change((e ? e.value : ""));
    }

    render() {
        var options = [
            {value: '&orderBy=travelDate&orderType=asc', label: 'Travel Date -Ascending'},
            {value: '&orderBy=travelDate&orderType=desc', label: 'Travel Date -Descending'},
            {value: '&orderBy=entryDate&orderType=asc', label: 'Entry Date - Ascending'},
            {value: '&orderBy=entryDate&orderType=desc', label: 'Entry Date - Descending'}
        ];

        return (
            <Select
                name="order"
                placeholder={"Sort"}
                options={options}
                value={this.state.value}
                inputProps={{'id': "orderBy"}}
                onChange={this.handleSortChange.bind(this)}
                searchable={false}
            />
        )


    }

}

export default  OrderByComponent;